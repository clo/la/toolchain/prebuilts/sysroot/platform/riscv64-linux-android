#!/bin/bash
set -e

# Copyright 2020 Google Inc. All rights reserved.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

if [ -z "$1" -o "$1" = "--help" ]; then
    echo "usage: $0 <build number>"
    exit 1
fi

readonly BUILD_NUMBER=$1

cd "$(dirname "$0")"

readonly tmpdir=$(mktemp -d)

function finish {
    if [ ! -z "${tmpdir}" ]; then
        rm -rf "${tmpdir}"
    fi
}
trap finish EXIT

function do_fetch_artifact() {
    if ! which fetch_artifact >/dev/null; then
        echo "install fetch_artifact using http://go/fetch_artifact#installing"
        exit 1
    fi
    # Work around http://b/151178103#comment18.
    mkdir -p "$HOME/.config/fetch_artifact"
    fetch_artifact --use_oauth2 --branch aosp-master --bid "${BUILD_NUMBER}" --target aosp_riscv64-userdebug "$1" "${tmpdir}/$1"
}

# Fetch artifacts.
do_fetch_artifact ndk_sysroot.tar.bz2
do_fetch_artifact bionic_sysroot_crt_objects_riscv64.zip

# Replace the usr directory.
rm -fr usr
tar -xf "${tmpdir}/ndk_sysroot.tar.bz2" --strip-components=2 ndk/sysroot
api_dir=usr/lib/riscv64-linux-android/10000
mkdir $api_dir
tar -xf "${tmpdir}/ndk_sysroot.tar.bz2" -C $api_dir --strip-components=6 ndk/platforms/android-current/arch-riscv64/usr/lib64
unzip -q -d $api_dir "${tmpdir}/bionic_sysroot_crt_objects_riscv64.zip"

# Commit it.
git add usr
git commit -m "Update riscv64-linux-android sysroot to ab/${BUILD_NUMBER}

https://ci.android.com/builds/branches/aosp-master/grid?head=${BUILD_NUMBER}&tail=${BUILD_NUMBER}

Test: treehugger"
